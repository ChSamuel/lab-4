from django.urls import re_path, path
from .views import *
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    path('blog', blog, name='blog'),
]
