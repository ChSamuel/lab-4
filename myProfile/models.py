from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Jadwal(models.Model):
    hari = models.CharField(max_length=6)
    tanggal = models.DateField()
    jam = models.TimeField()
    kegiatan = models.CharField(max_length=40)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=35)
