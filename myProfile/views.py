from django.shortcuts import render, redirect
from myProfile.models import Jadwal
from myProfile.forms import Add_To_Form

# Create your views here.
def blog(request):
    return render(request, "index.html")

def index(request):
    temp = Jadwal.objects.all()
    return render(request, "myWebsite.html", {"query" : temp})

def orgExperience(request):
    return render(request, "organizationExperiences.html")

def academicHistory(request):
    return render(request, "academicHistory.html")

def codingSkillset(request):
    return render(request, "codingSkillset.html")

def contactMe(request):
    return render(request, "contactMe.html")

def addSchedule(request):
    if request.method == "POST":
        form = Add_To_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("index")
    else:
        form = Add_To_Form()

    return render(request, "addSchedule.html", {'form' : form})

def deleteRow(request, schedule_id = None):
    temp = Jadwal.objects.get(id = schedule_id)
    temp.delete()
    return redirect("index")
