from django import forms
from myProfile.models import Jadwal

class Add_To_Form(forms.ModelForm):

    class Meta:
        model = Jadwal
        fields = {"hari", "tanggal", "jam", "kegiatan", "tempat", "kategori"}
        labels = {
                "hari" : "Isi hari di samping ini :",
                "tanggal" : "Isi tanggal di samping ini :",
                "jam" : "Isi jam di samping ini :",
                "kegiatan" : "Isi nama kegiatan :",
                "tempat" : "Isi nama tempat kegiatan :",
                "kategori" : "Isi jenis kategori kegiatan :"
            }
        
